import React, { useState } from 'react';
import './App.css';
import ChatList from './components/ChatList/ChatList';
import ChatRoom from './components/ChatRoom/ChatRoom';
import UserInfo from './components/UserInfo';
import ModalContactSelector from './components/ModalContactSelector/ModalContactSelector';

function App() {
  const [contactSelectorActive, setContactSelectorActive] = useState(false)
  return (
    <div className="chat-container">
      <div className="chat-left-side">
        <ChatList addChat={() => setContactSelectorActive(true)} />
      </div>
      <div className="chat-right-side">
        <ChatRoom />
        <UserInfo />
      </div>
      <ModalContactSelector active={contactSelectorActive} close={() => setContactSelectorActive(false)} />
    </div>
  );
}

export default App;
