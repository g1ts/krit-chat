import { createSlice } from '@reduxjs/toolkit';
import { v4 as uuidv4 } from 'uuid';

const initialState = {
    contacts: {},
    selectedContact: null
};

function createDemoData() {
    for (let i = 1; i <= 10; i++) {
        const id = uuidv4()
        initialState.contacts[id] = { id, nick: 'User ' + i, messages: [], avatar: '/img/avatar/avatar-person.svg' }
    }
}

createDemoData()

function createInitialMessages() {
    const messages = []
    for (let i = 1; i <= 5; i++) {
        messages.push({
            id: uuidv4(),
            incoming: true,
            time: Date.now(),
            text: `Сообщение от пользователя #${i} ${Math.random()}`
        })
        messages.push({
            id: uuidv4(),
            incoming: false,
            time: Date.now(),
            text: `Сообщение пользователю #${i} ${Math.random()}`
        })
    }
    return messages
}


export const tagsSlice = createSlice({
    name: 'contacts',
    initialState,
    reducers: {
        addMessage: (state, action) => {
            if (!state.selectedContact) return
            const newMessage = { id: uuidv4(), incoming: false, time: Date.now(), text: action.payload }
            state.contacts[state.selectedContact].messages.push(newMessage)
        },
        selectContact: (state, action) => {
            const contactID = action.payload
            if (!state.contacts[contactID].messages || state.contacts[contactID].messages.length === 0) {
                state.contacts[contactID].messages = createInitialMessages()
            }
            state.selectedContact = contactID
        }
    }
});

export const { addMessage, selectContact } = tagsSlice.actions;
export const selectContacts = (state) => Object.values(state.chat.contacts);
export const selectSelectedContact = (state) => state.chat.contacts[state.chat.selectedContact]
export const selectContactsWithDialogs = (state) => {
    const contactsWithDialogs = Object.values(state.chat.contacts).filter(contact => contact.messages && contact.messages.length > 0)
    /*
        порядок элементов в массиве messages важен, предполагается что они всегда будут в отсортированы заранее (при сохранении либо на стороне сервера)

        в задании написано "В каждом чате отображается последнее отправленное сообщение с датой." 
        из этого немогу понять имеется ввиду отправленное КЕМ сообщение
        по этому реализовал наиболее простой вариант когда отображается просто самое последнее сообщение в переписке не важно кем отправленное
    */
    return contactsWithDialogs.sort((contact1, contact2) => {
        const lastMessage1 = contact1.messages[contact1.messages.length - 1]
        const lastMessage2 = contact2.messages[contact2.messages.length - 1]
        return lastMessage2.time - lastMessage1.time
    })
}


export default tagsSlice.reducer;
