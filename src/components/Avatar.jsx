import React from 'react'

function Avatar({contact}) {
    return (
        <div className="dialog__user-avatar">
            <img src={contact.avatar} alt='' />
        </div>
    )
}

export default Avatar
