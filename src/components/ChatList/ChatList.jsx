import React from 'react'
import { useSelector } from 'react-redux'
import { selectContactsWithDialogs, selectSelectedContact } from '../../app/ChatSlice'
import Dialog from './Dialog'

function ChatList({ addChat }) {
    const contactsWithDialogs = useSelector(selectContactsWithDialogs)
    const selectedContact = useSelector(selectSelectedContact)
    return (
        <div className="chat-list">
            <div className="chat-list__top">
                <div className="add-chat-btn" onClick={addChat}>
                    <svg viewBox="0 0 24 24">
                        <path fill="#309d3b" d="M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M13,7H11V11H7V13H11V17H13V13H17V11H13V7Z" />
                    </svg>
                </div>
            </div>
            <div className="chat-list__body">
                {contactsWithDialogs.map(contact => <Dialog key={contact.id} contact={contact} selected={selectedContact.id === contact.id} />)}
            </div>
        </div>
    )
}

export default ChatList
