import classNames from 'classnames'
import { format } from 'date-fns'
import React from 'react'
import { useDispatch } from 'react-redux'
import { selectContact } from '../../app/ChatSlice'
import Avatar from '../Avatar'

function Dialog({contact, selected}) {
    const dispatch = useDispatch()
    const lastMessage = contact.messages[contact.messages.length-1]
    
    
    const handleClick = (event) => {
        dispatch(selectContact(contact.id))
    }

    const dialogClass = classNames('dialog', {'dialog_active': selected})

    return (
        <div className={dialogClass} onClick={handleClick}>
            <Avatar contact={contact} />
            <div className="dialog__text">
                <div className="dialog__username">{contact.nick}</div>
                <div className="dialog__last">
                    <div className="dialog__last-msg">{lastMessage.text}</div>
                    <div className="dialog__last-time">{format(lastMessage.time, 'HH:mm')}</div>
                </div>
            </div>
        </div>
    )
}

export default Dialog