import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { addMessage, selectSelectedContact } from '../../app/ChatSlice'
import Message from './Message'

function ChatRoom() {
    const dispatch = useDispatch()
    const contact = useSelector(selectSelectedContact)

    const [newMessage, setNewMessage] = useState('')

    function sendMessage() {
        if(newMessage==='') return
        dispatch(addMessage(newMessage))
        setNewMessage('')
    }
    const handleKeyDown = (event) => {
        if (event.key === 'Enter') { sendMessage() }
    }
    const handleChange = (event) => {
        setNewMessage(event.target.value)
    }

    return (
        <div className="chat-room">
            <div className="chat-room__messages">
                {contact && contact.messages.map(message => <Message key={message.id} message={message} />)}
            </div>
            <div className="chat-room__footer">
                <input type="text" className="input-field" placeholder="Write Message ..." value={newMessage} onKeyDown={handleKeyDown} onChange={handleChange} />
                <div className="send-btn" onClick={sendMessage}>
                    <svg viewBox="0 0 24 24">
                        <path fill="#3b86c6" d="M2,21L23,12L2,3V10L17,12L2,14V21Z" />
                    </svg>
                </div>
            </div>
        </div>
    )
}

export default ChatRoom
