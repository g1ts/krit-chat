import classNames from 'classnames'
import React from 'react'

function Message({ message }) {
    const messageClass = message.incoming ? 'incoming-msg' : 'received_msg'
    const messageTextClass = classNames("message", message.incoming ? 'message_in' : 'message_out')

    return (
        <div className={messageClass}>
            <div className={messageTextClass}>{message.text}</div>
        </div>
    )
}

export default Message
