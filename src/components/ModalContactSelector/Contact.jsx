import React from 'react'
import Avatar from '../Avatar'

function Contact({contact, onClick}) {
    return (
        <div className="dialog" onClick={onClick}>
            <Avatar contact={contact} />
            <div className="dialog__text">
                <div className="dialog__username">{contact.nick}</div>
            </div>
        </div>
    )
}

export default Contact
