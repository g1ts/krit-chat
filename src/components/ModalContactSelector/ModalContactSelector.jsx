import classNames from 'classnames'
import React from 'react'
import { useDispatch } from 'react-redux'
import { useSelector } from 'react-redux'
import { selectContact, selectContacts } from '../../app/ChatSlice'
import Contact from './Contact'

function ModalContactSelector({ active, close }) {
    const dispatch = useDispatch()
    const contacts = useSelector(selectContacts)
    const handleClickOnContact = (contact) => () => {
        dispatch(selectContact(contact.id))
    }
    const modalClass = classNames({ active, 'modal-backdrop': true })
    return (
        <div className={modalClass} onClick={close}>
            <div className="modal">
                <div className="modal__head">
                    <div className="modal__title">Choose contact</div>
                    <div className="modal__close" onClick={close}>&times;</div>
                </div>
                <div className="modal__body">
                    {contacts.map(contact => <Contact key={contact.id} contact={contact} onClick={handleClickOnContact(contact)} />)}
                </div>
            </div>
        </div>
    )
}

export default ModalContactSelector
